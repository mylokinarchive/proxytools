import pygeoip

class GeoLocator(object):
    gi = pygeoip.GeoIP('./GeoIP.dat')

    def __init__(self, proxy):
        self.host = proxy[0]
        self.port = proxy[1]
        
    @property
    def country(self):
        return self.gi.country_name_by_addr(self.host)
    
    @property
    def code(self):
        return self.gi.country_code_by_addr(self.host)
    
country = lambda proxy: GeoLocator(proxy).country
code = lambda proxy: GeoLocator(proxy).code
