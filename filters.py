import urllib2
import socket
import logging


import geo
import ext
import ping

log = logging.getLogger('proxytool.filters')
socket.setdefaulttimeout(15)

@ext.async
def host(in_queue, out_queue, host, sync):
    def condition(proxy):
        proxy= urllib2.ProxyHandler({'http': '%s:%s'%(proxy[0], proxy[1])})
        opener = urllib2.build_opener(proxy)
        urllib2.install_opener(opener)
        try:
            status = urllib2.urlopen(host)
        except:
            return False
        else:
            return True
    while sync.is_set():
        proxy = in_queue.get()
        if condition(proxy):
            log.debug('Proxy %s valid for host: %s'%(proxy, host))
            out_queue.put(proxy)
        else:
            log.debug('Proxy %s invalid for host: %s'%(proxy, host))

@ext.async            
def country(in_queue, out_queue, country, sync):
    condition = lambda proxy: True if geo.country(proxy)==country.title() else False
    while sync.is_set():
        proxy = in_queue.get()
        if condition(proxy):
            log.debug('Proxy %s valid for country: %s'%(proxy, country))
            out_queue.put(proxy)
        else:
            log.debug('Proxy %s invalid for country: %s'%(proxy, country))

@ext.async            
def code(in_queue, out_queue, code, sync):
    condition = lambda proxy: True if geo.code(proxy)==code.upper() else False
    while sync.is_set():
        proxy = in_queue.get()
        if condition(proxy):
            log.debug('Proxy %s valid for country code: %s'%(proxy, code))
            out_queue.put(proxy)
        else:
            log.debug('Proxy %s invalid for country code: %s'%(proxy, code))

@ext.async     
def latency(in_queue, out_queue, latency, sync):
    def condition(proxy):
        proxy_latency = ping.simple_ping(proxy[0])
        if proxy_latency and proxy_latency<=latency:
            return proxy_latency
        else:
            return 5000
    while sync.is_set():
        proxy = in_queue.get()
        proxy_latency = condition(proxy)
        if proxy_latency:
            log.debug('Proxy %s valid for latency: %s'%(proxy, latency))
            out_queue.put((proxy_latency, proxy))
        else:
            log.debug('Proxy %s invalid for latency: %s'%(proxy, delay))