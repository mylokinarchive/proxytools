import itertools
import Queue
import threading
import time

import provider
import filters

proxies = (provider.prov1(), provider.prov2())

class ProxyCloud():
    def __init__(self, proxies):
        self.chain = itertools.chain(*proxies)
        self.sync = threading.Event()
    
    @property
    def queue(self):
        queue = Queue.Queue()
        for proxy in self.chain:
            queue.put(proxy)
        return queue
        
    def filter(self, host=None, country=None, code=None, latency=None):
        queue = self.queue        
        queue_host = None
        queue_country = None
        queue_code = None
        queue_latency = None

        self.sync.set()
        
        if host:
            queue_host = Queue.Queue()
            for x in xrange(100):
                filters.host(queue, queue_host, host, self.sync)            
        else:
            queue_host = queue
            
        if country:
            queue_country = Queue.Queue()
            filters.country(queue_host, queue_country, country, self.sync)
        else:
            queue_country = queue_host

        if code:
            queue_code = Queue.Queue()
            filters.code(queue_country, queue_code, code, self.sync)
        else:
            queue_code = queue_country
        
        if latency:
            queue_latency = Queue.PriorityQueue()
            filters.latency(queue_code, queue_latency, latency, self.sync)
        else:
            queue_latency = queue_code
            
        return queue_latency

    def host(self, host, quality=3, min_latency=3000):
        queue = self.filter(host=host, latency=min_latency)
        while True:
            if queue.qsize()>=quality:
                self.sync.clear()
                return self.proxy_repr(queue.get()[1])
            time.sleep(0.01)
            
    @staticmethod
    def proxy_repr(proxy):
        return 'http://%s:%s/'%(proxy[0], proxy[1])