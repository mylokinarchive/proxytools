import re
import urllib2


def prov1():
    content = urllib2.urlopen('http://nntime.com/').read()
    kw = {}
    for k, v in re.findall('(?P<key>\w{1})=(?P<value>\d{1});', content):
        kw[k] = v
    proxylist = []
    for host, port in re.findall('<td>(?P<host>[12]?\d{1,2}\.[12]?\d{1,2}\.[12]?\d{1,2}\.[12]?\d{1,2}).+(?P<port>\+\w\+\w\+\w\+\w)', content):            
        port = int(''.join([kw[k] for k in port.replace('+', '')]))
        proxylist.append((host, port))
    return proxylist

def prov2():
    content = urllib2.urlopen('http://www.freeproxy.ru/download/lists/goodproxy.txt').read()
    proxylist = []
    for host, port in re.findall('(?P<host>[12]?\d{1,2}\.[12]?\d{1,2}\.[12]?\d{1,2}\.[12]?\d{1,2}):(?P<port>\d\d\d\d)', content):
        proxylist.append((host, int(port)))
    return proxylist
