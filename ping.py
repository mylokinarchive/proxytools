import urllib
import datetime

def simple_ping(host):
    t1 = datetime.datetime.now()
    urllib.urlopen('http://%s'%host)
    t2 = datetime.datetime.now()
    t = t2-t1
    return t.seconds*1000+t.microseconds/1000

if __name__=='__main__':
    print simple_ping("ya.ru")