import optparse
import logging
import urlparse

import cloud

proxycloud = cloud.ProxyCloud(cloud.proxies)
logging.basicConfig(level=logging.DEBUG)

parser = optparse.OptionParser()
parser.add_option('-u', '--url', dest='url', help='pick up proxy for url')
parser.add_option('-q', '--quality', dest='quality', type='int', help='quality of choice')

(options, args) = parser.parse_args()

print proxycloud.host(options.url, quality=options.quality)